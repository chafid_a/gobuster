#base image
FROM golang:1.11 AS build

#add maintainer info
LABEL maintainer="Chafid Ahmad <chafid@gmail.com>"

#set working directory inside container
ADD . /home/chafid/SRLabs/gobuster
WORKDIR /home/chafid/SRLabs/gobuster

ARG GOPATH
ENV GOPATH=$GOPATH
ARG GOBIN
ENV GOBIN=$GOBIN
ARG GOROOT
ENV GOROOT=$GOROOT 

#RUN apt-get update
#RUN apt-get -y install git
RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o gobuster .

FROM alpine:latest AS runtime
#copy from everything from current dir to pwd
COPY --from=build /home/chafid/SRLabs/gobuster /home/chafid/SRLabs/gobuster
WORKDIR /home/chafid/SRLabs/gobuster
#run gobuster --help
RUN ls .
RUN chmod +x /home/chafid/SRLabs/gobuster/gobuster
#ENTRYPOINT ["sh", "-c" , "/home/chafid/SRLabs/gobuster --help"]
CMD ["/home/chafid/SRLabs/gobuster/gobuster"]
